//
//  test_counter.c
//  counter
//
//  Created by Danny Goossen on 19/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//
#include <stdio.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/file.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include <stdarg.h>
#include <limits.h>
#include <syslog.h>
#include <signal.h>
#include <getopt.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <errno.h>

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

#ifndef GITVERSION
#define GITVERSION "no_git"
#endif

/* Socket name of the counter*/
#ifndef SRV_SOCK
#define SRV_SOCK "count.sock"
#endif

/* Directory to put client socket in */
#ifndef SOCK_DIR
#define SOCK_DIR "sockets"
#endif

/* default verbose */
#ifndef VERBOSE_YN
#define VERBOSE_YN 1
#endif

/* default 100k repeats */
#ifndef REPEATS
#define REPEATS 100000LLu
#endif

#define CMD_RESET 1
#define CMD_RESULT 2
#define CMD_COUNT 3
#define CMD_STRESS 4

/* default 5 sec timeout */
#define TIME_OUT 50LLu

char local_sock_path[512];

struct parameter_t
{
	char socket_dir[256];
	char cli_socket[256];
	char srv_socket[256];
	char srv_socket_path[512];
	u_int8_t verbose;
	u_int8_t quiet;
	u_int8_t debug;
    u_int8_t strip_result;
	int32_t timeout;
	u_int64_t repeats;
	
};

static struct parameter_t parameters;

void usage( int exitcode)
{
	fprintf(stderr, "Usage: test_counter [--options]  <COMMAND>  \n");
	fprintf(stderr, "          [options]     \n\n");
	fprintf(stderr, "version          : Shows the version and usage\n");
	fprintf(stderr, "help             : Shows the version and usage\n");
	fprintf(stderr, "verbose          : turns on verbose output mode\n");
	fprintf(stderr, "debug            : turns on verbose output mode\n");
	fprintf(stderr, "quiet            : quiet down output\n");
	fprintf(stderr, "sock_dir=sockets     : specifies the directory to look or write unix sockets\n");
	fprintf(stderr, "srv_sock=count.sock  : specifies to which unix socket we comunicate\n");
	fprintf(stderr, "cli_sock=client.sock : specifies the client socket name, default <hostname>.sock\n");
	fprintf(stderr, "repeats=10000    : the number of repeats for stress test\n");
	fprintf(stderr, "timeout=10       : specifies the timeout in 100 milliseconds for a command/connect\n");
	fprintf(stderr, "strip            : will output result without newline \n");
	fprintf(stderr, "\n");
	fprintf(stderr, "Defaults: verbose    = %d\n",   VERBOSE_YN);
	fprintf(stderr, "          count_sock  = %s\n",   SRV_SOCK);
	fprintf(stderr, "          sock_dir   = %s\n",   SOCK_DIR);
	fprintf(stderr, "          repeats    = %llu\n",  REPEATS);
	fprintf(stderr, "          timeout    = %llu\n",  TIME_OUT);
	
	fprintf(stderr, "\n");
	fprintf(stderr, "COMMANDS:\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "stress          : stress test, repeats  \n");
	fprintf(stderr, "reset           : reset counter to 0\n");
	fprintf(stderr, "result          : get counter, without increments\n");
	fprintf(stderr, "count <default> : increment counter and get result\n");
	
	exit(exitcode);
}


/*------------------------------------------------------------------------
 * int process_options(int argc, char **argv, tb_parameter_t *parameter)
 *
 * Process the program argument options and set the parameters accordingly
 *------------------------------------------------------------------------*/
int process_options(int argc, char **argv, struct parameter_t *parameter)
{
	
	struct option long_options[] = {
		{ "verbose",    0, NULL, 'v' },
		{ "quiet",      0, NULL, 'q' },
		{ "srv_sock",   1, NULL, 'n' },
		{ "sock_dir",   1, NULL, 'D' },
		{ "cli_sock",   1, NULL, 'c' },
		{ "help",       0, NULL, 'j' },
		{ "version",    0, NULL, 'g' },
		{ "repeats",    1, NULL, 'r' },
		{ "debug",      0, NULL, 'd' },
		{ "timeout",    1, NULL, 't' },
      { "strip",    0, NULL, 'S' },
		{ NULL,         0, NULL, 0 } };
	
	int           which;
	optind=1;
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long(argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameter->quiet)
					{
						fprintf(stderr,"Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					{
						parameter->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameter->verbose)
					{
						fprintf(stderr,"Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
						parameter->quiet = 1;
				}
					break;
				case 't': {
					parameter->timeout   = atoi(optarg);
				}
					break;
				case 'n': {
					sprintf(parameter->srv_socket,"%s",optarg);
				}
					break;
				case 'c': {
					
					sprintf(parameter->cli_socket,"%s",optarg);
				}
					break;
				case 'D': {
					sprintf(parameter->socket_dir,"%s",optarg);
				}
					break;
				case 'r': {
					parameter->repeats   = atoi(optarg);
				}
					break;
				case 'g':
				{
					
					printf("%s\n",GITVERSION);
					exit(0);
				}
				case 'j': {
					printf("\ntest_counter Git Version: %s\n \n",GITVERSION);
					usage(0);
					break;
				}
				case 'd': {
					parameter->debug = 1;
				}
               break;
            case 'S': {
               parameter->strip_result = 1;
            }
               break;
					/* otherwise    : display usage information */
				default:
					;
					usage(1);
			}
		}
	}
	// if NULL then no option was given and we default define the dir where tblastd was started !!!!!!
	//value = getenv(name);
	return optind;
}

/*------------------------------------------------------------------------
 * void rset_missing_parms(tb_parameter_t *parameter)
 *
 * set the parameters to default if not in commandline or config
 *------------------------------------------------------------------------*/
void set_missing_parms(struct parameter_t *parameter)
{
	if (strlen(parameter->srv_socket) == 0)
	{
		sprintf(parameter->srv_socket,"%s",SRV_SOCK);
	}
	
	if (strlen(parameter->socket_dir) == 0)
	{
		sprintf(parameter->socket_dir,"%s",SOCK_DIR);
	}
	
	if (parameter->repeats==0) parameter->repeats  =  REPEATS;
	if (parameter->timeout==0) parameter->timeout  =  TIME_OUT;
	if (! parameter->verbose && !parameter->quiet) parameter->verbose = VERBOSE_YN;
	
	if (strlen(parameter->cli_socket) == 0)
	{
		char Name[150];
		memset(Name, 0, 150);
		gethostname(Name, 150);
		sprintf(parameter->cli_socket,"%s.sock",Name);
	}
	sprintf(local_sock_path,"%s/%s",parameter->socket_dir,parameter->cli_socket);
	sprintf(parameter->srv_socket_path,"%s/%s",parameter->socket_dir,parameter->srv_socket);
	return;
}

static void signal_handler(sig)
int sig;
{
   int i;
   switch(sig) {
      case SIGHUP:
         break;
      case SIGSTOP:
      case SIGQUIT:
      case SIGTERM:
      case SIGKILL:
      case SIGINT:
         fprintf (stderr,"\nEXIT: Signal %d : %s !!!\n",sig,strsignal(sig));
         for (i=getdtablesize();i>=2;--i)
         {
            close(i); /* close all non std descriptors */
         }
         unlink(local_sock_path);
         exit(1);
         break;
   }
}

void init_signals()
{
   signal(SIGHUP,&signal_handler); /* catch hub signal */
   signal(SIGTERM,&signal_handler); /* catch term signal */
   signal(SIGSTOP,&signal_handler); /* catch stop signal */
   signal(SIGINT, &signal_handler); /* catch int signal */
   signal(SIGQUIT, &signal_handler); /* catch quit signal */
   signal( SIGKILL, &signal_handler); /* catch kill signal */
}

/*
******************************************
 	connect
******************************************
 */
int connect_socket( struct parameter_t * parameters,char * sock_client)
{
	int sockid;
	struct sockaddr_un client_addr,serv_addr;
	
	bzero((char *) &serv_addr, sizeof(serv_addr));
	bzero((char *) &client_addr, sizeof(client_addr));
	
	client_addr.sun_family = AF_UNIX;
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, parameters->srv_socket_path);
	
	int servlen=(int)strlen(serv_addr.sun_path) + 1 + (int)sizeof(serv_addr.sun_family);
	
	client_addr.sun_family = AF_UNIX;
	strcpy(client_addr.sun_path, sock_client);
	int clilen=(int)strlen(client_addr.sun_path) + 1 + (int)sizeof(client_addr.sun_family);
	
	sockid=socket(AF_UNIX,SOCK_DGRAM,0);
	if (bind(sockid,(struct sockaddr *)&client_addr,clilen)==SOCKET_ERROR)
	{
		perror("bind");
		close(sockid);
		unlink(sock_client);
		return (INVALID_SOCKET);
	}
	u_int64_t timeout=parameters->timeout;
	int result=0;
	do {
		result=connect(sockid, (struct sockaddr *) &serv_addr, servlen);
		timeout--;
		usleep(100000);
	} while(result==SOCKET_ERROR && timeout!=0);
	if (result==SOCKET_ERROR)
	{
      perror("Connect");
		close(sockid);
		unlink(sock_client);
		sockid=INVALID_SOCKET;
	}
	return sockid;
}

// ********************************************************************
//          MAIN
// ********************************************************************
int main(int argc, char **argv, char** envp)
{
      bzero(&parameters, sizeof(parameters));
   unsigned long long count;
   int this_error=0;
   size_t read_size;
   size_t write_size;
  
   size_t count_len=sizeof(count);
	
	int st_no_option_args=0;
	if ((st_no_option_args=process_options(argc, argv,&parameters)) < 0)
	{
		fprintf(stderr, " *** ERROR in command line options, exit \n");
		return (-1);
	}
	// fix this, need the commands for thisone
	
	int no_option_args=argc-st_no_option_args;
	set_missing_parms(&parameters);
   init_signals();
	int command=0;
	if (no_option_args >= 1)
	{
		
		if (strcmp("reset",argv[st_no_option_args]) ==0) command =CMD_RESET;
		if (strcmp("result",argv[st_no_option_args]) ==0) command =CMD_RESULT;
		if (strcmp("count",argv[st_no_option_args]) ==0) command =CMD_COUNT;
	   if (strcmp("stress",argv[st_no_option_args]) ==0) command =CMD_STRESS;
   }

   if (parameters.verbose) fprintf(stderr,"Connect from [%s]/ %s to %s\n",parameters.socket_dir,parameters.cli_socket,parameters.srv_socket);
	
   int sockid=connect_socket(&parameters,local_sock_path);
   if (sockid==INVALID_SOCKET)
   {
      if (parameters.verbose) fprintf(stderr, "exit\n");
      return (-1);
   }
   u_int64_t i;
   u_int64_t repeats=1;
   char request='+';
   if (command==CMD_RESET) request='0';
   if (command==CMD_RESULT) request='=';
   if (command==CMD_STRESS) repeats=parameters.repeats;
   
   struct timeval tv;
   tv.tv_sec = parameters.timeout;
   tv.tv_usec = 0;
   fd_set rds,wds;
   int sfd=sockid;
   int s_ret=0;
   int s_err=0;
   if (parameters.verbose) fprintf(stderr,"Connected\n");
   for (i=0;i<repeats;i++)
   {
      do
      {
		write_size = send(sockid,&request, 1,MSG_DONTWAIT);
		if ( write_size==SOCKET_ERROR ) this_error=errno; else this_error=0;
		if ( write_size==SOCKET_ERROR  && (this_error==EINTR || this_error==EAGAIN || this_error==EWOULDBLOCK) )
		{
			 usleep(1);
			 FD_ZERO( &wds );
			 FD_SET( sockid, &wds );
			 tv.tv_sec = parameters.timeout/10;
			 tv.tv_usec = 100000 * parameters.timeout%10;
			 s_ret=select(sfd+1, NULL,&wds, NULL,  &tv);
			 if (s_ret==SOCKET_ERROR)
			 {
				s_err=errno;
				if (s_err!=EINTR && s_err!=EAGAIN && this_error!=EWOULDBLOCK)
				{
				   fprintf(stderr,"Select Error write: %s\n", strerror(s_err));
				}
				else {
					if (parameters.debug) fprintf(stderr,"Select non error: %s\n", strerror(s_err));
					usleep(1);
				}
			}
			 else if (s_ret>0)
			 {
				s_err=0;
				write_size = send(sockid,&request, 1,MSG_DONTWAIT);
				 if ( write_size==SOCKET_ERROR ) this_error=errno; else this_error=0;
				if ( write_size==SOCKET_ERROR && this_error!=EINTR && this_error!=EAGAIN  && this_error!=EWOULDBLOCK )
				{
				   fprintf(stderr,"Write error request: %s\n",strerror(this_error));
				   s_ret=SOCKET_ERROR; // exit while with error
					s_err=this_error;
				}
				if ( write_size==SOCKET_ERROR && (this_error==EAGAIN  || this_error==EWOULDBLOCK || this_error==EINTR) )
				{
					usleep(1);
					if (parameters.debug) fprintf(stderr,"write non error: %s\n", strerror(this_error));
					s_ret=SOCKET_ERROR;
					s_err=this_error;
				}
			 }
			 else
			 {//timeout write
				fprintf(stderr,"Write Timeout error Request\n");
				 s_ret=SOCKET_ERROR;
				 s_err=SOCKET_ERROR; // force break
			 }
		}
		else if ( write_size==SOCKET_ERROR)
		{
			fprintf(stderr,"direct Write error request: %s\n",strerror(this_error));
			s_ret=SOCKET_ERROR; // exit while with error
			s_err=this_error;
		
		}
		else s_ret=1;
	  } while (s_ret==SOCKET_ERROR &&( s_err==EINTR || s_err==EAGAIN || s_err==EWOULDBLOCK) );
	  
	  //usleep(1); // can avoid going into the select
	   
      // if no error read result
      if (s_ret==SOCKET_ERROR || s_ret==0)
      {
         break; // break for loop
      }
      else do
      {
		  read_size = recv(sockid,&count,count_len, MSG_DONTWAIT );
		  if ( read_size==SOCKET_ERROR ) this_error=errno; else this_error=0;
		  if ( read_size==SOCKET_ERROR && (this_error==EINTR || this_error==EAGAIN || this_error==EWOULDBLOCK))
		  {
			 FD_ZERO( &rds );
			 FD_SET( sockid, &rds );
			 tv.tv_sec = parameters.timeout/10;
			 tv.tv_usec = 100000 *parameters.timeout%10;
			 s_ret=select(sfd+1, &rds, NULL, NULL,  &tv);
			 if (s_ret==SOCKET_ERROR)
			 {
				 s_err=errno;
				 if (s_err!=EINTR && this_error!=EAGAIN && this_error!=EWOULDBLOCK)
				 {
					 fprintf(stderr,"Select Error Read: %s", strerror(s_err));
					 break;
				 }
				 else
				 {
					 if (parameters.debug) fprintf(stderr,"Select non Error Read: %s", strerror(s_err));
					 usleep(1);
				 }
			 }
			 else if (s_ret>0)
			 {
				s_err=0;
				read_size = recv(sockid,&count,count_len, MSG_DONTWAIT );
				if ( read_size==SOCKET_ERROR ) this_error=errno; else this_error=0;
				if ( read_size==SOCKET_ERROR && (this_error==EAGAIN  || this_error==EWOULDBLOCK || this_error==EINTR)  )
				{
					usleep(1);
					if (parameters.debug) fprintf(stderr,"Read non error result: %s\n",strerror(this_error));
					s_ret=SOCKET_ERROR; // set s_ret so we loop again with and EAGAIN or EW...
					s_err=this_error;
				}
				if ( read_size==SOCKET_ERROR && this_error!=EINTR && this_error!=EAGAIN && this_error!=EWOULDBLOCK )
				{
				   fprintf(stderr,"Read error result: %s\n",strerror(this_error));
				   s_ret=SOCKET_ERROR;
				   s_err=this_error;

				}
				else if (read_size > 0)
				{
				   if ((parameters.verbose || command==CMD_RESULT) && command!=CMD_STRESS)
				   {
					  if (parameters.strip_result==1 )
						 fprintf(stderr, "%lld",count);
					  else
						 fprintf(stderr, "%lld\n",count);
				   }
				}
			 }
			 else
			 {//timeout
				fprintf(stderr,"Time Out Read result\n");
			 }
		  }
		  else if ( read_size==SOCKET_ERROR)
		  {
			  fprintf(stderr,"Read error result: %s\n",strerror(this_error));
			  s_ret=SOCKET_ERROR;
			  s_ret=this_error;
		  }
		  else
		  {
			  s_ret=1;
		  }
      } while (s_ret==SOCKET_ERROR &&( s_err==EINTR || s_err==EAGAIN || s_err==EWOULDBLOCK));
      if (s_ret==SOCKET_ERROR || s_ret==0) break; // break for loop
   }
   close(sockid);
   unlink(local_sock_path);
	if (parameters.verbose || command==CMD_STRESS) fprintf(stderr,"Done\n");
   if (s_ret==SOCKET_ERROR || s_ret==0)
      return 1;
   else
      return 0;
}
