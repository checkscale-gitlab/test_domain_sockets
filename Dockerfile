FROM scratch
MAINTAINER danny.goossen@gioxa.com

ADD counter /
VOLUME /sockets
ENTRYPOINT ["/counter"]
